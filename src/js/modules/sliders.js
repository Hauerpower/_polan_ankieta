export  default function slidersInit(){
	
	(function($){
	
		$(document).ready(function(){
			$('.slider-for').slick({
				slidesToShow: 4,
				slidesToScroll: 1,
				arrows: false,
				fade: true,
				asNavFor: '.slider-nav'
			  });
			  $('.slider-nav').slick({
				slidesToShow: 4,
				slidesToScroll: 4,
				asNavFor: '.slider-for',
				dots: false,
				arrows: false,
			  });

		  });

	})(jQuery);
	
}
 